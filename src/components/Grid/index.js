import React, { Component } from "react";
import "./style.scss";

class Grid extends Component {
  render() {
    return (
     <div className="row">
         <div className="col-half">
             {this.props.children}
         </div>
         <div className="col-half">
             {this.props.children}
         </div>
     </div>
    );
  }
}
 
export default Grid;