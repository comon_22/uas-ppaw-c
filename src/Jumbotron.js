import React, { Component } from "react";
import "./css/bootstrap.min.css"
import "./style.css"

class Jumbotron extends Component {
    render() {
        return (
            <div>
                <div class="judul">
                    <h2>ECO-FRIENDLY<br />
                        PEST CONTROL.</h2>
                    <div class="teks">
                        <p><span>Low risk, non-toxic pesticides that is safe for kids and pets! We care about you, your family and also the environment.</span></p>
                    </div>
                    <div class="tombol">
                        <button type="button" class="btn btn-primary">LEARN MORE</button>
                    </div>
                </div>
            </div>

        )
    }
}

export default Jumbotron;