import React, { Component } from "react";
import "./style.css";
import Image from './assets/monsterpest.png';

class Header extends Component {
    render() {
        return (
            <div>
                <div className="inner">
                    <div className="kiri">
                        <p>CALL US TODAY FOR FREE ESTIMATE: <span>(786)488-7235</span></p>

                    </div>
                    <div className="kanan">
                        <img src="https://www.miamipestcontrolsolutions.com/wp-content/uploads/2017/04/envelope.png" className="enpelove img-responsive" />
                        <p>FREE INSPECTION <span>MONSTERPESTCONTROL@LIVE.COM</span></p>
                    </div>
                    <div className="clear"></div>
                </div>
                <div className="topnav">
                    <img src={Image} alt="Monster Pest Control" />
                    <div className="nav">
                        <a className="active" href="#home">HOME</a>
                        <a href="#news">ABOUT US</a>
                        <a href="#contact">SERVICES</a>
                        <a href="#">SPECIALS</a>
                        <a href="#">BLOG</a>
                        <a href="#">PEST LIBRARY</a>
                        <a href="#">CONTACT US</a>
                    </div>
                </div>
            </div>
        );
    }
}

export default Header;