import React, { Component } from "react";
import { Route, NavLink, HashRouter } from "react-router-dom";
import Header from "./Header";
import Section from "./Section"

class Main extends Component {
  render() {
    return (
      <div>
        <div className="badan">
          <Header></Header>
          <Section></Section>
        </div>
      </div>

    );
  }
}

export default Main;